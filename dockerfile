FROM alpine:3.12

RUN apk --no-cache add php7 php7-fpm php7-opcache php7-mysqli php7-json php7-openssl php7-curl php7-zlib php7-xml php7-phar php7-intl php7-dom php7-xmlreader php7-ctype php7-session php7-mbstring php7-gd nginx supervisor curl
RUN rm /etc/nginx/conf.d/default.conf

COPY includes/nginx/nginx.conf /etc/nginx/nginx.conf
COPY includes/php/ioncube/loader.so /usr/lib/php7/modules/ioncube_loader_lin_7.3.so
COPY includes/php/ioncube/ioncube.conf /etc/php7/conf.d/00-ioncube.ini
COPY includes/php/pool/pool.conf /etc/php7/php-fpm.d/www.conf
COPY includes/php/php.conf /etc/php7/conf.d/custom.ini
COPY includes/supervisord/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

EXPOSE 65534
EXPOSE 65535

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]